set nocompatible
set encoding=utf-8

let mapleader="\\"

source ~/.vim/plugins.vim
source ~/.vim/theme.vim
source ~/.vim/functions.vim
source ~/.vim/general.vim

set ttyfast
set lazyredraw
set timeoutlen=1000 ttimeoutlen=0
